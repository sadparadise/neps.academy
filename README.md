# neps.academy

Este repositório contém minhas soluções para exercícios diversos do site [neps.academy](https://neps.academy/).

Cada arquivo dele contém uma solução em C++ e é nomeado seguindo o padrão <ins>num</ins>-<ins>nome</ins>[<ins>_extra</ins>].cpp, sendo <ins>num</ins> o número do exercício no site (que pode ser conferido pela sua URL), <ins>nome</ins> uma única palavra que faz referência ao título do exercício e <ins>_extra</ins> um trecho adicional que contém a descrição de uma versão alternativa de uma solução.
