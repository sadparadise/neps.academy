#include <iostream>
#define N_MAX 10000
using namespace std;

int N, T;
int H[N_MAX];
string A[N_MAX], O[N_MAX]; // Alunos, Lista ordenada por habilidade

int main(void)
{
	cin >> N >> T;
	for (int i = 0; i < N; ++i)
	{
		cin >> A[i] >> H[i];
	}

	for(int i = 0; i < N; ++i) // Preenchimento da lista ordenada
	{
		int max = -1, ind = -1;
		for (int j = 0; j < N; ++j)
			if (H[j] > max)
			{
				max = H[j];
				ind = j;
			}
		H[ind] = -1;
		O[i] = A[ind];
	}

	for (int i = 0; i < T; ++i)
	{
		int l = 0, ind = -1;
		string pri = "{"; // "{" é usado por vir logo depois do "z" na tabela ASCII e por ser, assim, considerado posterior a qualquer nome na ordem alfabética
		string time[N_MAX];
		cout << "Time " << i + 1 << endl;
		for (int j = i; j < N; j += T)
			time[l++] = O[j]; // Membros de um mesmo time ficarão guardados nesse vetor temporário
		for (int j = 0; j < l; ++j)
		{
			for (int k = 0; k < l; ++k)
				if (time[k].compare(pri) < 0) // Mesmo processo de ordenamento, mas das strings
				{
					pri = time[k];
					ind = k;
				}
			cout << pri << endl;
			time[ind] = "{";
			pri = "{";
		}
	}

	return 0;
}
