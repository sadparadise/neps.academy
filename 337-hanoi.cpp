#include <iostream>
#include <cmath>
#include <list>
using namespace std;

int main ()
{
    list<int> Ns;
    int i = 1;
    for (;;)
    {
        int N;
        cin >> N;
		if (N == 0) break;
        Ns.push_back(N);
    }
	for (int N : Ns)
	{
		cout << "Teste " << i++ << endl;
        cout << (long)(pow (2, N) - 10) << endl << endl;
	}
    return 0;
}
