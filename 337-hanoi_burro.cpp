#include <iostream>
#include <list>
using namespace std;

long moves;

void hanoi(int n, int o, int d, int t)
{
    if (n == 1)
    {
    	++moves;
    	return;
    }
    else
    {
        hanoi(n-1, o, t, d);
        ++moves;
        hanoi(n-1, t, d, o);
    }
}

int main ()
{
    list<int> Ns;
    int i = 1;
    for (;;)
    {
    	int N;
    	cin >> N;
    	if (N == 0) break;
		Ns.push_back(N);
    }

	for (int N : Ns)
	{
    	moves = 0;
    	hanoi(N, 0, 2, 1);
		cout << "Teste " << i++ << endl;
        cout << moves << endl << endl;
	}
    return 0;
}


